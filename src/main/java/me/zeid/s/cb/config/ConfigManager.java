/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public abstract class ConfigManager {
 private final String[] KNOWN_CATEGORIES;
 
 private Map<String,ConfigurationSection> categories;
 private Map<String,String>               filenames;
 private File                             dataFolder;
 private Plugin                           plugin;
 
 public ConfigManager(String[] categories) throws ConfigManagerException {
  this(new String[0], null, null);
 }
 public ConfigManager(String[] categories, File dataFolder) throws ConfigManagerException {
  this(categories, dataFolder, null);
 }
 public ConfigManager(String[] categories, Plugin plugin) throws ConfigManagerException {
  this(categories, null, plugin);
 }
 public ConfigManager(File dataFolder) throws ConfigManagerException {
  this(null, dataFolder); 
 }
 public ConfigManager(Plugin plugin) throws ConfigManagerException {
  this(null, plugin);
 }
 private ConfigManager(String[] categories, File dataFolder, Plugin plugin)
         throws ConfigManagerException {
  this.KNOWN_CATEGORIES = categories;
  
  this.categories  = new HashMap<String,ConfigurationSection>();
  this.filenames   = new HashMap<String,String>();
  this.plugin      = plugin;
  
  if (this.plugin != null)
   this.dataFolder = this.plugin.getDataFolder();
  else if (dataFolder != null)
   this.dataFolder = dataFolder;
  else
   this.dataFolder = new File(System.getProperty("user.dir"));
  
  this.load();
 }
 
 public ConfigurationSection get(String category) {
  return this.get(category, null);
 }
 
 public ConfigurationSection get(String category, String section) {
  category = category.trim().toLowerCase();
  ConfigurationSection categorySection = this.categories.get(category);
  if (categorySection != null && section != null)
   return categorySection.getConfigurationSection(section);
  return categorySection;
 }
 
 public String getDefaultCategory() {
  if (KNOWN_CATEGORIES.length > 0)
   return KNOWN_CATEGORIES[0];
  return null;
 }
 
 public boolean isCategory(String category) {
  category = category.trim().toLowerCase();
  for (String knownCategory : KNOWN_CATEGORIES) {
   if (knownCategory.equals(category))
    return true;
  }
  return false;
 }
 
 public ConfigManager load() throws ConfigManagerException {
  for (String category : KNOWN_CATEGORIES) {
   this.load(category);
  }
  return this;
 }
 public ConfigManager load(String category)
                      throws ConfigManagerException {
  if (category == null)
   return this.load();
  
  category = category.trim().toLowerCase();
  this.instantiateCategory(category);
  return this;
 }
 protected abstract void instantiateCategory(String category)
                         throws ConfigManagerException;
 protected ConfigManager load(String category, ConfigurationSection section,
                              ConfigurationSection... defaults) {
  if (defaults != null && defaults.length > 0)
   this.addDefaults(section, defaults);
  this.categories.put(category, section);
  return this;
 }
 protected ConfigManager load(String category, FileConfiguration section, String filename,
                              ConfigurationSection... defaults)
                         throws ConfigManagerException {
  return this.load(category, section, filename, defaults, true);
 }
 private ConfigManager load(String category, FileConfiguration section, String filename,
                            ConfigurationSection[] defaults, boolean firstCall)
                       throws ConfigManagerException {
  if (firstCall) {
   // Add default values to section
   if (section instanceof YamlConfiguration) {
    // Add YAML file defaults to the beginning of `defaults` if applicable
    InputStream defaultsStream = this.getResource(filename);
    if (defaultsStream != null) {
     List<ConfigurationSection> defaultsList = new ArrayList<ConfigurationSection>();
     defaultsList.add(YamlConfiguration.loadConfiguration(defaultsStream));
     if (defaults != null)
      defaultsList.addAll(Arrays.asList(defaults));
     defaults = defaultsList.toArray(new ConfigurationSection[0]);
    }
   }
   this.addDefaults(section, defaults);
  }
  try {
   if (filename != null) {
    File file = new File(this.getDataFolder(), filename);
    try {
     section.load(file);
    } catch (FileNotFoundException e) {
     if (firstCall) {
      // Create file if necessary
      this.getDataFolder().mkdirs();
      if (section instanceof DirectoryConfiguration) {
       file.mkdirs();
      } else {
       InputStream stream = this.getResource(filename);
       if (stream != null)
        this.saveResource(filename, false);
       else
        file.createNewFile();
      }
      return this.load(category, section, filename, defaults, false);
     } else
      throw new ConfigManagerException("Could not create " + filename, e);
    }
    this.filenames.put(category, filename);
   }
   return this.load(category, section);
  } catch (InvalidConfigurationException e) {
   throw new ConfigManagerException(e);
  } catch (IOException e) {
   throw new ConfigManagerException(e);
  }
 }
 private ConfigManager addDefaults(ConfigurationSection category,
                                   ConfigurationSection... defaults) {
  if (category != null && defaults != null) {
   for (ConfigurationSection defaultSection : defaults) {
    if (defaultSection != null) {
     ConfigurationSection defaultDefaults = defaultSection.getDefaultSection();
     if (defaultDefaults != null) {
      for (String key : defaultDefaults.getKeys(true))
       category.addDefault(key, defaultDefaults.get(key));
     }
     for (String key : defaultSection.getKeys(true))
      category.addDefault(key, defaultSection.get(key));
    }
   }
  }
  return this;
 }
 
 public ConfigManager reload() throws ConfigManagerException {
  return this.load();
 }
 public ConfigManager reload(String category)
                      throws ConfigManagerException {
  return this.load(category);
 }
 
 public String filename(String category) {
  category = category.trim().toLowerCase();
  ConfigurationSection section = this.categories.get(category);
  if (section instanceof FileConfiguration && this.filenames.containsKey(category))
   return this.filenames.get(category);
  return null;
 }
 
 public ConfigManager save() throws ConfigManagerException {
  for (String category : this.categories.keySet()) {
   this.save(category);
  }
  return this;
 }
 public ConfigManager save(String category) throws ConfigManagerException {
  return this.save(category, null);
 }
 public ConfigManager save(String category, String subsection)
                      throws ConfigManagerException {
  if (category == null)
   return this.save();
  category = category.trim().toLowerCase();
  ConfigurationSection section = this.categories.get(category);
  if (section instanceof FileConfiguration && this.filenames.containsKey(category)) {
   try {
    File file = new File(this.getDataFolder(), this.filenames.get(category));
    if (subsection != null && section instanceof DirectoryConfiguration)
     ((DirectoryConfiguration) section).save(file, subsection);
    else
     ((FileConfiguration) section).save(file);
   } catch (IOException e) {
    throw new ConfigManagerException(e);
   }
  }
  return this;
 }
 
 public File getDataFolder() {
  return this.dataFolder;
 }
 public Plugin getPlugin() {
  return this.plugin;
 }
 public InputStream getResource(String filename) {
  if (plugin != null)
   return plugin.getResource(filename);
  return null;
 }
 public void saveResource(String resourcePath, boolean replace) {
  if (plugin != null)
   plugin.saveResource(resourcePath, replace);
 }
}
