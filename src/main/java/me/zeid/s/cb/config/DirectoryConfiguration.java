/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class DirectoryConfiguration extends FileConfiguration {
 public DirectoryConfiguration() {
  super();
 }
 public DirectoryConfiguration(char separator) {
  super();
  this.options().pathSeparator(separator);
 }
 
 // Working loaders
 public void load(String directory) throws InvalidConfigurationException, IOException {
  this.load(new File(directory));
 }
 public void load(File directory) throws InvalidConfigurationException, IOException {
  directory = directory.getCanonicalFile();
  if (!directory.isDirectory())
   throw new FileNotFoundException("\"" + directory.getPath() + "\" is not a directory");
  for (File file : directory.listFiles()) {
   String filename = file.getName();
   if (filename.endsWith(".yml")) {
    String sectionName = filename.replaceAll("\\.yml$", "");
    FileConfiguration sectionConfig = new YamlConfiguration();
    sectionConfig.options().pathSeparator(this.options().pathSeparator());
    sectionConfig.load(file);
    this.putSection(sectionName, sectionConfig);
   }
  }
 }
 protected void putSection(String name, ConfigurationSection section) {
  this.createSection(name, section.getValues(true));
 }
 // No-op loaders; this is a directory-based configuration, so these are meaningless
 public void load(InputStream stream) {}
 public void loadFromString(String contents) {}
 
 // Working savers
 public void save(String directory) throws IOException { this.save(directory, null); }
 public void save(String directory, String section) throws IOException {
  this.save(new File(directory), section);
 }
 public void save(File directory) throws IOException { this.save(directory, null); }
 public void save(File directory, String section) throws IOException {
  directory = directory.getCanonicalFile();
  if (!directory.isDirectory())
   throw new FileNotFoundException("\"" + directory.getPath() + "\" is not a directory");
  for (String sectionName : this.getKeys(false)) {
   if (section != null && !sectionName.equals(section))
    continue;
   if (this.isConfigurationSection(sectionName)) {
    FileConfiguration sectionConfig = new YamlConfiguration();
    sectionConfig.addDefaults(this.getConfigurationSection(sectionName).getValues(true));
    sectionConfig.options().copyDefaults(true);
    sectionConfig.save(new File(directory, sectionName + ".yml"));
   }
  }
 }
 // No-op saver; this is a directory-based configuration, so this is meaningless
 public String saveToString() { return ""; }
 
 protected String buildHeader() { return ""; }
 
 // Use global defaults as the defaults for top-level sections
 public ConfigurationSection createSection(String path) {
  ConfigurationSection section = super.createSection(path);
  if (section != null && section.getParent() == this) {
   Configuration defaults = this.getDefaults();
   if (defaults != null) {
    for (String key : defaults.getKeys(true))
     section.addDefault(key, defaults.get(key));
   }
  }
  return section;
 }
}

