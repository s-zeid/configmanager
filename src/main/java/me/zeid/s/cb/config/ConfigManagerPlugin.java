/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   CraftBnay-BITS CraftBukkit Plugin
   Custom logic for the Bnay Intricate Transit System on the CraftBnay
   Minecraft server.
   
   Copyright (C) 2013 Scott Zeid
   https://craft.bnay.me/bits
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.config;

import java.lang.InstantiationException;
import java.lang.IllegalAccessException;
import java.lang.IllegalArgumentException;
import java.lang.NoSuchMethodException;
import java.lang.SecurityException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class ConfigManagerPlugin extends JavaPlugin implements CMPlugin {
 private ConfigManager                  configManager;
 private Class<? extends ConfigManager> configManagerClass;
 
 public ConfigManagerPlugin() {
  this(null);
 }
 public ConfigManagerPlugin(Class<? extends ConfigManager> configManagerClass) {
  this.configManager      = null;
  this.configManagerClass = configManagerClass;
 }
 
 protected void setConfigManager(ConfigManager configManager) {
  try {
   if (configManager == null) {
    if (configManagerClass == null)
     throw new ConfigManagerException("configManager is null and no ConfigManager"
                                     +" subclass was given in the plugin constructor");
    try {
     Constructor<? extends ConfigManager> constructor;
     constructor = configManagerClass.getConstructor(Plugin.class);
     configManager = constructor.newInstance(this);
    } catch (NoSuchMethodException e) {
     throw new ConfigManagerException(e);
    } catch (SecurityException e) {
     throw new ConfigManagerException(e);
    } catch (InstantiationException e) {
     throw new ConfigManagerException(e);
    } catch (IllegalAccessException e) {
     throw new ConfigManagerException(e);
    } catch (IllegalArgumentException e) {
     throw new ConfigManagerException(e);
    } catch (InvocationTargetException e) {
     throw new ConfigManagerException(e);
    }
   }
   this.configManager = configManager;
  } catch (ConfigManagerException e) {
   this.getLogger().log(Level.SEVERE, "Error loading configuration",
                        (e.getCause() == null) ? e.getCause() : e);
  }
 }
 
 public ConfigManager getConfigManager() {
  if (this.configManager == null)
   this.setConfigManager(null);
  return this.configManager;
 }
 
 public ConfigurationSection getConfig(String category) {
  return this.getConfig(category, null);
 }
 public ConfigurationSection getConfig(String category, String section) {
  return this.getConfigManager().get(category, section);
 }
 
 public void reloadConfig() {
  this.reloadConfig(null);
 }
 public void reloadConfig(String category) {
  try {
   this.getConfigManager().reload(category);
  } catch (ConfigManagerException e) {
   this.getLogger().log(Level.SEVERE, "Error reloading configuration",
                        (e.getCause() == null) ? e.getCause() : e);
  }
 }
 
 public void saveConfig() {
  this.saveConfig(null);
 }
 public void saveConfig(String category) {
  this.saveConfig(category, null);
 }
 public void saveConfig(String category, String subsection) {
  try {
   this.getConfigManager().save(category, subsection);
  } catch (ConfigManagerException e) {
   this.getLogger().log(Level.SEVERE, "Error saving configuration",
                        (e.getCause() == null) ? e.getCause() : e);
  }
 }
 
 // Compatibility with stock Bukkit getConfig()
 public FileConfiguration getConfig() {
  String defaultCategory = this.getConfigManager().getDefaultCategory();
  if (defaultCategory != null) {
   ConfigurationSection section = this.getConfig(defaultCategory);
   if (section instanceof FileConfiguration)
    return (FileConfiguration) section;
  }
  return null;
 }
}
